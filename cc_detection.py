import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from sklearn.cross_validation import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix,auc,roc_curve,classification_report
from sklearn import svm
from imblearn.over_sampling import SMOTE
from imblearn.under_sampling import RandomUnderSampler
from xgboost import XGBClassifier   

def data_prepration(x): # preparing data for training and testing as we are going to use different data
    #again and again so make a function
    x_features= x.ix[:,x.columns != "Class"]
    x_labels=x.ix[:,x.columns=="Class"]
    x_features_train,x_features_test,x_labels_train,x_labels_test = train_test_split(x_features,x_labels,test_size=0.2)
    print("length of training data")
    print(len(x_features_train))
    print("length of test data")
    print(len(x_features_test))
    return(x_features_train,x_features_test,x_labels_train,x_labels_test)

## first make a model function for modeling with confusion matrix
def model(model,features_train,features_test,labels_train,labels_test):
    clf= model
    clf.fit(features_train,labels_train.values.ravel())
    pred=clf.predict_proba(features_test)[:,1]
    #evaluateModel(labelsTest, pred)
    return (labels_test,pred)

def modelResampled(clf, featuresTrain, featuresTest, labelsTrain, labelsTest, rate):
    resampledFeatures, resampledLabels = resampleTrain(featuresTrain, labelsTrain, rate)
    return model(clf, resampledFeatures, featuresTest, resampledLabels, labelsTest)


def printRatio(data):
    nrNormalTransactions = len(data[data["Class"]==0])
    nrFraudTransactions = len(data[data["Class"]==1])
    print("normal transactions: ", nrNormalTransactions, " / fraud transactions: ", nrFraudTransactions)
    print("percentage of normal transactions:" , nrNormalTransactions/(nrNormalTransactions+nrFraudTransactions))

def resampleTrain(featuresTrain, labelsTrain, rate):
    trainSize = 40000;
    undersampling = RandomUnderSampler(random_state=0,ratio={0:int(rate * trainSize)})
    os = SMOTE(random_state=0, ratio={1: int((1 - rate) * trainSize)})

    usFeaturesTrain, usLabelsTrain = undersampling.fit_sample(featuresTrain, labelsTrain)
    osFeaturesTrain, osLabelsTrain = os.fit_sample(usFeaturesTrain, usLabelsTrain)
    osFeaturesTrain = pd.DataFrame(data=osFeaturesTrain,columns=featuresTrain.columns )
    osLabelsTrain= pd.DataFrame(data=osLabelsTrain,columns=["Class"])
    return (osFeaturesTrain, osLabelsTrain)

def compareResults(results, labels):
    lw =2
    fig = plt.figure()

    for k in results:
        labels, pred = results[k]
        evaluation = [ int(x > 0.5) for x in pred]
        cnf_matrix=confusion_matrix(labels,evaluation)
        print("\n------------------", k, "----------------")
        print(cnf_matrix)
        print(classification_report(labels, evaluation, target_names=["non-fraud","fraud"]))
        fpr, tpr, _ = roc_curve(labels,pred)
        roc_auc = auc(fpr, tpr)
        plt.plot(fpr, tpr, lw=lw, label=k + ' (AUC = %0.2f)' % roc_auc)
#        print(k, " precision: ", precision_score(labels, pred), recall_score(labels, pred))


    plt.plot([0, 1], [0, 1], color='navy', lw=lw, linestyle='--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic example')
    plt.legend(loc="lower right")
    fig.show()
    input()


data = pd.read_csv("../creditcard.csv", header=0)
featuresTrain, featuresTest, labelsTrain, labelsTest = data_prepration(data)

clf = LogisticRegression(verbose=0)
xgclf = XGBClassifier(n_jobs=-1)

results = {};
results["LogisticRegression 80/20"] = modelResampled(clf, featuresTrain, featuresTest, labelsTrain, labelsTest, 0.8)
results["LogisticRegression 50/50"] = modelResampled(clf, featuresTrain, featuresTest, labelsTrain, labelsTest, 0.5)
results["XGBoost 80/20"] = modelResampled(xgclf,featuresTrain, featuresTest, labelsTrain, labelsTest, 0.8)
results["XGBoost 50/50"] = modelResampled(xgclf,featuresTrain, featuresTest, labelsTrain, labelsTest, 0.5)
results["LogisticRegression default"] = model(clf, featuresTrain, featuresTest, labelsTrain, labelsTest)

compareResults(results, labelsTest)

# output resampled testdata:
#pd.concat([osFeaturesTrain.join(osLabelsTrain),featuresTest.join(labelsTest)]).to_csv("concat.csv")
